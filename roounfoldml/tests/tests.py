import roounfoldml
import pandas as pd

dataobs = pd.read_csv('~/dataobs.csv', index_col=0)
simobs = pd.read_csv('~/simobs.csv', index_col=0)
genobs = pd.read_csv('~/genobs.csv', index_col=0)
truthobs = pd.read_csv('~/truthobs.csv', index_col=0)

print(simobs.shape)
print('making response')
r = roounfoldml.RooUnfoldMLResponse(simobs[:1000], genobs[:1000])

def test_regression():
    print('using response to define roounfold regression')
    reg = roounfoldml.RooUnfoldRegression(r, dataobs)
    print('now trying to unfold')
    regression = reg.Vunfold()
    assert regression

def test_binned_regression():
    # print('using response to define binned roounfold regression')
    # breg = roounfoldml.RooUnfoldBinnedRegression(r, dataobs)
    # print('now trying to unfold')
    # binnedregression = breg.Hunfold()
    assert True

def test_omnifold():
    print('using response to define omnifold reweighting')
    omni = roounfoldml.RooUnfoldReweight(r, dataobs)
    print('now trying to unfold')
    omnifold = omni.Vunfold()
    assert omnifold


def test_cinn():
    print('using response to define cINN')
    c = roounfoldml.RooUnfoldCondition(r, dataobs)
    print('now trying to unfold')
    cinn = c.Vunfold()
    assert cinn
