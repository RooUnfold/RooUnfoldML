import sys
import numpy as np
import pandas as pd
import tensorflow as tf
import ROOT

from .network_modules import ConditionalFlow
from .dataloader import sanitise

@tf.function
def nll_loss(model, optimiser, batch_gen, batch_sim):
    with tf.GradientTape() as tape:
        loss = -tf.reduce_mean(model.log_prob(batch_gen, context=batch_sim))
    gradients = tape.gradient(loss, model.trainable_weights)
    optimiser.apply_gradients(zip(gradients, model.trainable_weights))

    return loss


class RooUnfoldCondition:
    def __init__(self, response, data, parameters=None):
        data = sanitise(data)
        assert data.shape[1] == response.n_input_features, "input features for data and response do not match"
        self.response = response
        self.data = data
        if parameters is None:
            meta = {
                "units": 16,
                "layers": 4,
                "initializer": "glorot_uniform",
                "activation": "leakyrelu",
            }
        self.model = ConditionalFlow(dims_in=[self.response.n_input_features],
                                     dims_c=[[self.response.n_target_features]],
                                     n_blocks=12,
                                     subnet_meta=meta)
        self.compiled = False
        self.unfolded_data = None

        
    def fit_model(self, EPOCHS=500, BATCH_SIZE = 1000, batch = True):
        sim = tf.convert_to_tensor(self.response.sim, dtype=tf.float32)
        gen = tf.convert_to_tensor(self.response.gen, dtype=tf.float32)

        if len(gen)<BATCH_SIZE and batch:
            print("less data than batch size, disabling batch")
            batch = False
        
        if batch:
            train_dataset = tf.data.Dataset.from_tensor_slices((gen, sim))
            train_dataset = train_dataset.shuffle(
                buffer_size=500000).batch(
                    BATCH_SIZE).prefetch(tf.data.AUTOTUNE)
        
        # set up models etc
        lr_schedule = tf.keras.optimizers.schedules.InverseTimeDecay(5e-3,
                                                                     len(gen)//BATCH_SIZE,
                                                                     0.1)
        opt = tf.keras.optimizers.Adam(lr_schedule)
        for e in range(EPOCHS):
            if not batch:
                loss = nll_loss(self.model, opt, gen, [sim])
            else:
                batch_losses = []
                for step, (batch_gen, batch_sim) in enumerate(train_dataset):
                    batch_loss = nll_loss(self.model, opt, batch_gen, [batch_sim])
                    batch_losses.append(batch_loss)
                loss = tf.reduce_mean(batch_losses)
            if e%int(EPOCHS/5)==0:
                print('epoch {} has loss {}'.format(e,loss))

                    
        self.compiled = True

        
    def unfold(self):
        if not self.compiled:
            self.fit_model()
        data = tf.convert_to_tensor(self.data, dtype=tf.float32)
        #not sure why this is necessary...
        data1,data2 = np.split(data, 2)
        np1 = self.model.sample(int(5e5),[data1]).numpy()
        np2 = self.model.sample(int(5e5),[data2]).numpy()        
        unfolded = np.concatenate([np1,np2])
        self.unfolded_data = unfolded
        
    def Vunfold(self):
        if self.unfolded_data is None:
            self.unfold()
        return self.unfolded_data
    
    
    def Hunfold(self, feature, nbins=None, xmin=None, xmax=None):
        """returns histogram of unfolded feature"""
        if self.unfolded_data is None:
            self.unfold()
        if self.response.truth_labels:
            df = pd.DataFrame(data = self.unfolded_data, columns = self.response.truth_keys)
        else:
            print("I don't know which column that is, your response wasn't labeled.")       

        # if variable bins but a list
        if isinstance(nbins, list):
            nbins = np.asarray(nbins)
        
        # now if we have a variable width histogram.
        if isinstance(nbins,np.ndarray):
            bin_edges = nbins
            nbins = len(bin_edges)-1
            h = ROOT.TH1F("","",nbins,bin_edges)
    
        elif xmin or xmin==0:
            h = ROOT.TH1F("", "", nbins, xmin, xmax)
            bin_edges = np.linspace(xmin, xmax, nbins+1)
            
        elif nbins is None:
            _,bin_edges = np.histogram(df[feature]) #just get bin edges
            h = ROOT.TH1F("","",len(bin_edges)-1,bin_edges[0],bin_edges[-1])

        else:
            print("should be feature, nbins, xmin, xmax")
            print("or feature, bin_edges")
            print('or just feature')
            return
        
        bin_contents, _ = np.histogram(df[feature].values, bin_edges)
        [h.SetBinContent(i+1,b) for i,b in enumerate(bin_contents)]
        return h
