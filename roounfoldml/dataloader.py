import pandas as pd
import ROOT

def sanitise(data):
    if isinstance(data, ROOT.TTree):
        data = pd.DataFrame(data = data.AsMatrix(),
                           columns = [i.GetName() for i in data.GetListOfBranches()])
    elif isinstance(data, ROOT.RDataFrame):
        columns = [i for i in data.GetColumnNames() if len(i)>0]
        data = pd.DataFrame(data.AsNumpy(columns))
    return data
        
