import pandas as pd
import ROOT

class RooUnfoldMLResponse:
    def __init__(self, sim, gen):
        if isinstance(gen, ROOT.TTree):
            sim = pd.DataFrame(data = sim.AsMatrix(),
                                  columns = [i.GetName() for i in sim.GetListOfBranches()])
            gen = pd.DataFrame(data = gen.AsMatrix(),
                               columns = [i.GetName() for i in gen.GetListOfBranches()])
        elif isinstance(gen, ROOT.RDataFrame):
            columns = [i for i in gen.GetColumnNames() if len(i)>0]
            sim = pd.DataFrame(sim.AsNumpy(columns))
            gen = pd.DataFrame(gen.AsNumpy(columns))

        assert gen.shape[0] == sim.shape[0], "should be a 1:1 mapping of simulated:generated events"
        self.sim = sim
        self.gen = gen
        self.n_input_features = sim.shape[1]
        self.n_target_features = gen.shape[1]
        self.truth_labels = False
        
        if isinstance(gen, pd.DataFrame):
            self.truth_labels = True
            self.truth_keys = gen.keys()
