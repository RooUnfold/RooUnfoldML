# roounfoldml/simple.py

import tensorflow as tf
import pandas as pd
import ROOT
import numpy as np
from .dataloader import sanitise


class RooUnfoldRegression:
    def __init__(self, response, data, parameters=None):
        self.response = response
        self.data = sanitise(data)
        if parameters is None:
            self.model = self.build_default()
        self.compiled = False
        self.unfolded_data = None


    def build_default(self):
        """build a simple default regression model with regularisation"""

        #define layers
        layer0 = tf.keras.layers.Dense(units=50,
                                       input_shape=[self.response.n_input_features],
                                       activation='relu')
        layer1 = tf.keras.layers.Dense(units=50,
                                       activity_regularizer=tf.keras.regularizers.l1_l2(l1=1e-5, l2=1e-3),
                                       kernel_regularizer=tf.keras.regularizers.l1_l2(l1=1e-5, l2=1e-3))
        layer2 = tf.keras.layers.Dense(units=50)
        layer3 = tf.keras.layers.Dense(units=self.response.n_target_features)

        return tf.keras.Sequential([layer0, layer1, layer2, layer3])


    def fit_model(self):
        """compile the model using keras and fit it"""

        # some helper functions
        early = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=10)

        # compile the model - Should take options from params, with defaults in __init__
        self.model.compile(optimizer="adam",
                           loss='mean_squared_logarithmic_error',
                           metrics=["mse"])

        # fit the model using early stopping - Also options from above
        self.model.fit(self.response.sim,
                       self.response.gen,
                       batch_size=100,
                       epochs=100,
                       shuffle=False,
                       callbacks=[early],
                       verbose=0)

        self.compiled = True


    def unfold(self):
        """predicts data using model"""
        if not self.compiled:
            self.fit_model()
        self.unfolded_data = self.model.predict(self.data)


    def Vunfold(self):
        """returns vector of unfolded events"""
        if self.unfolded_data is None:
            self.unfold()
        if self.response.truth_labels:
            return pd.DataFrame(data = self.unfolded_data, columns = self.response.truth_keys)
        else:
            return self.unfolded_data

         
    def Hunfold(self, feature, nbins=None, xmin=None, xmax=None):
        """returns histogram of unfolded feature"""
        if self.unfolded_data is None:
            self.unfold()
        if self.response.truth_labels:
            df = pd.DataFrame(data = self.unfolded_data, columns = self.response.truth_keys)
        else:
            print("I don't know which column that is, your response wasn't labeled.")       

        # if variable bins but a list
        if isinstance(nbins, list):
            nbins = np.asarray(nbins)
        
        if isinstance(nbins,np.ndarray):
            bin_edges = nbins
            nbins = len(bin_edges)-1
            h = ROOT.TH1F("","",nbins,bin_edges)
    
        elif xmin or xmin==0:
            h = ROOT.TH1F("", "", nbins, xmin, xmax)
            bin_edges = np.linspace(xmin, xmax, nbins+1)
            
        elif nbins is None:
            _,bin_edges = np.histogram(df[feature])
            h = ROOT.TH1F("","",len(bin_edges)-1,bin_edges[0],bin_edges[-1])

        else:
            print("should be feature, nbins, xmin, xmax")
            print("or feature, bin_edges")
            print('or just feature')
            return
        
        bin_contents, _ = np.histogram(df[feature].values, bin_edges)
        [h.SetBinContent(i+1,b) for i,b in enumerate(bin_contents)]
        return h


class RooUnfoldBinnedRegression:
    def __init__(self, response, data, parameters=None):
        data = sanitise(data)
        assert data.shape[1] == response.n_input_features, "input features for data and response do not match"
        self.response = response
        self.data = data
        if parameters is None:
            self.n_bins = 50
            self.model = self.build_model()
        self.compiled = False
        self.unfolded_data = None

    def build_model(self):
        # Define layer
        layer0 = tf.keras.layers.Dense(units=50, input_shape=[self.response.n_input_features])
        layer1 = tf.keras.layers.Dense(units=50,
                                       activity_regularizer=tf.keras.regularizers.l1_l2(l1=1e-5, l2=1e-5),
                                       kernel_regularizer=tf.keras.regularizers.l1_l2(l1=1e-5, l2=1e-5))
        layer2 = tf.keras.layers.Dense(units=50)
        layer3 = tf.keras.layers.Dense(units=self.n_bins * self.response.n_target_features, activation='softmax')
        return tf.keras.Sequential([layer0, layer1, layer2, layer3])

    def fit_model(self):
        binned_target = self.normalize_and_bin()
        early = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=3)
        self.model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['mse'])
        self.model.fit(tf.keras.utils.normalize(self.response.sim), binned_target, batch_size=100, epochs=30,
                       shuffle=False, callbacks=[early], verbose=0)
        self.compiled = True

    def normalize_and_bin(self):
        gen = self.response.gen
        self.min_gen = gen.min()
        self.max_gen = gen.max()
        zeroed = gen - gen.min()
        normed = zeroed / zeroed.max() * .999 * self.n_bins
        return tf.keras.utils.to_categorical(normed, self.n_bins)

    def unfold(self):
        if not self.compiled:
            self.fit_model()
        self.unfolded_data = self.model.predict(tf.keras.utils.normalize(self.data))

    def Hunfold(self):
        if self.unfolded_data is None:
            self.unfold()
        return self.unfolded_data.sum(axis=0)
