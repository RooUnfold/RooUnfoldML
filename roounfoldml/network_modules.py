## Based on work by Ramon Winterhalder 
## Modified from code published here
## https://github.com/ramonpeter/UnbinnedMeasurements

import tensorflow as tf
import numpy as np
from typing import Dict, Union, Tuple, Iterable
from scipy.stats import special_ortho_group

class AllInOneBlock(tf.keras.layers.Layer):
    """Module combining the most common operations in a normalizing flow or
    similar model. It combines affine coupling, permutation, and
    global affine transformation ('ActNorm'). It can also be used as
    GIN coupling block and use an inverted pre-permutation.
    The affine transformation includes a soft clamping mechanism,
    first used in Real-NVP.
    """
    def __init__(
        self,
        dims_in: Tuple[int],
        dims_c: Iterable[Tuple[int]] = None,
        subnet_meta: Dict = None,
        subnet_constructor: callable = None,
        clamp: float = 2.0,
        gin_block: bool = False,
        global_affine_init: float = 1.0,
        global_affine_type: str = "SOFTPLUS",
        permute_soft: bool = False,
        reverse_permutation: bool = False,
        seed: Union[int, None] = None,
    ):
        """
        Args:
          subnet_meta:
            meta defining the structure of the subnet like number of layers,
            units, activation functions etc.
          subnet_constructor:
            class or callable ``f``, called as
            ``f(meta, channels_in, channels_out)`` and
            should return a tf.keras.layer.Layer.
            Predicts coupling coefficients :math:`s, t`.
          clamp:
            Soft clamping for the multiplicative component. The
            amplification or attenuation of each input dimension can be at most
            exp(±clamp).
          gin_block:
            Turn the block into a GIN block from Sorrenson et al, 2019.
            Makes it so that the coupling operations as a whole is
            volume preserving.
          global_affine_init:
            Initial value for the global affine scaling.
          global_affine_type:
            ``'SIGMOID'``, ``'SOFTPLUS'``, or ``'EXP'``. Defines the activation
            to be used on the beta for the global affine scaling.
          permute_soft:
            bool, whether to sample the permutation matrix `R` from `SO(N)`,
            or to use hard permutations instead. Note, ``permute_soft=True``
            is very slow when working with >512 dimensions.
          reverse_permutation:
            Reverse the permutation before the block, as introduced by Putzky
            et al, 2019. Turns on the :math:`R^{-1}` pre-multiplication above.
          seed:
            Int seed for the permutation (numpy is used for RNG). If seed is
            None, do not reseed RNG.
        """
        super().__init__()
        if dims_c is None:
            dims_c = []
        self.dims_in = tuple(dims_in)
        self.dims_c = list(dims_c)

        self.channels = self.dims_in[-1]
        # rank of the tensors means 1d, 2d, 3d tensor etc.
        self.input_rank = len(self.dims_in) - 1

        # tuple containing all dims except for
        # batch-dim (used at various points)
        self.sum_dims = tuple(range(1, 2 + self.input_rank))

        assert all(
            [
                tuple(self.dims_c[i][:-1]) == tuple(self.dims_in[:-1])
                for i in range(len(self.dims_c))
            ]
        ), "Dimensions of input and one or more conditions don't agree."
        self.conditional = len(self.dims_c) > 0
        self.condition_length = sum(
            [self.dims_c[i][-1] for i in range(len(self.dims_c))]
        )

        split_len1 = self.channels // 2
        split_len2 = self.channels - self.channels // 2
        self.splits = [split_len1, split_len2]

        self.permute_function = lambda x, w: tf.linalg.matvec(w, x, transpose_a=True)

        self.clamp = clamp
        self.GIN = gin_block
        self.reverse_pre_permute = reverse_permutation

        # global_scale is used as the initial value for the global affine scale
        # (pre-activation). It is computed such that
        # global_scale_activation(global_scale) = global_affine_init
        # the 'magic numbers' (specifically for sigmoid) scale the activation to
        # a sensible range.
        if global_affine_type == "SIGMOID":
            global_scale = 2.0 - np.log(10.0 / global_affine_init - 1.0)
            self.global_scale_activation = lambda a: 10 * tf.sigmoid(a - 2.0)
        elif global_affine_type == "SOFTPLUS":
            global_scale = 2.0 * np.log(np.exp(0.5 * 10.0 * global_affine_init) - 1)
            self.global_scale_activation = (
                lambda a: 0.1 * 2.0 * tf.math.softplus(0.5 * a)
            )
        elif global_affine_type == "EXP":
            global_scale = np.log(global_affine_init)
            self.global_scale_activation = tf.exp
        else:
            raise ValueError(
                'Global affine activation must be "SIGMOID", "SOFTPLUS" or "EXP"'
            )

        self.global_scale = self.add_weight(
            "global_scale",
            shape=(1, *([1] * self.input_rank), self.channels),
            initializer=tf.keras.initializers.Constant(global_scale),
            trainable=True,
        )

        self.global_offset = self.add_weight(
            "global_offset",
            shape=(1, *([1] * self.input_rank), self.channels),
            initializer=tf.keras.initializers.Zeros(),
            trainable=True,
        )

        # Define the permutation matrix
        # either some random rotation matrix
        # or a hard permutation instead.
        if permute_soft and self.channels > 512:
            warnings.warn(
                (
                    "Soft permutation will take a very long time to initialize "
                    f"with {self.channels} feature channels. Consider using hard permutation instead."
                )
            )

        if permute_soft:
            w = special_ortho_group.rvs(self.channels, random_state=seed)
        else:
            np.random.seed(seed)
            w = np.zeros((self.channels, self.channels))
            for i, j in enumerate(np.random.permutation(self.channels)):
                w[i, j] = 1.0

        self.w_perm = self.add_weight(
            "w_perm",
            shape=(*([1] * self.input_rank), self.channels, self.channels),
            initializer=tf.keras.initializers.Constant(w),
            trainable=False,
        )

        self.w_perm_inv = self.add_weight(
            "w_perm_inv",
            shape=(*([1] * self.input_rank), self.channels, self.channels),
            initializer=tf.keras.initializers.Constant(w.T),
            trainable=False,
        )

        if subnet_constructor is None:
            raise ValueError(
                "Please supply a callable subnet_constructor"
                "function or object (see docstring)"
            )
        
        self.subnet = subnet_constructor(
            subnet_meta, self.splits[0] + self.condition_length, 2 * self.splits[1]
        )
        self.last_jac = None

    def _permute(self, x, rev=False):
        """Performs the permutation and scaling after the coupling operation.
        Returns transformed outputs and the LogJacDet of the scaling operation."""
        if self.GIN:
            scale = 1.0
            perm_log_jac = 0.0
        else:
            scale = self.global_scale_activation(self.global_scale)
            perm_log_jac = tf.reduce_sum((tf.math.log(scale)), axis=self.sum_dims)

        if rev:
            x_permute = (
                self.permute_function(x, self.w_perm_inv) - self.global_offset
            ) / scale
            return x_permute, perm_log_jac

        x_permute = self.permute_function(x * scale + self.global_offset, self.w_perm)
        return x_permute, perm_log_jac

    def _pre_permute(self, x, rev=False):
        """Permutes before the coupling block, only used if
        reverse_permutation is set"""
        if rev:
            return self.permute_function(x, self.w_perm)

        return self.permute_function(x, self.w_perm_inv)

    def _affine(self, x, a, rev=False):
        """Given the passive half, and the pre-activation outputs of the
        coupling subnetwork, perform the affine coupling operation.
        Returns both the transformed inputs and the LogJacDet."""

        # the entire coupling coefficient tensor is scaled down by a
        # factor of ten for stability and easier initialization.
        a *= 0.1
        s, t = tf.split(a, [self.splits[0], self.splits[1]], -1)
        sub_jac = self.clamp * tf.math.tanh(s)
        if self.GIN:
            sub_jac -= tf.reduce_mean(sub_jac, axis=self.sum_dims, keepdims=True)

        if not rev:
            return x * tf.exp(sub_jac) + t, tf.reduce_sum(sub_jac, axis=self.sum_dims)

        return (x - t) * tf.exp(-sub_jac), -tf.reduce_sum(sub_jac, axis=self.sum_dims)

    def call(self, x, c=None, rev=False, jac=True):
        """See base class docstring"""
        if rev:
            x, global_scaling_jac = self._permute(x, rev=True)
        elif self.reverse_pre_permute:
            x = self._pre_permute(x, rev=False)

        x1, x2 = tf.split(x, self.splits, axis=-1)

        if self.conditional:
            x1c = tf.concat([x1, *c], -1)
        else:
            x1c = x1

        if not rev:
            a1 = self.subnet(x1c)
            x2, j2 = self._affine(x2, a1)
        else:
            a1 = self.subnet(x1c)
            x2, j2 = self._affine(x2, a1, rev=True)

        log_jac_det = j2
        x_out = tf.concat([x1, x2], -1)

        if not rev:
            x_out, global_scaling_jac = self._permute(x_out, rev=False)
        elif self.reverse_pre_permute:
            x_out = self._pre_permute(x_out, rev=True)

        # add the global scaling Jacobian to the total.
        # trick to get the total number of non-channel dimensions:
        # number of elements of the first channel of the first batch member
        # Fix n_pixels
        #n_pixels = tf.size(x) / self.channels
        log_jac_det += (-1) ** rev  * global_scaling_jac
        
        if not jac:
            return x_out
        
        return x_out, log_jac_det



    def get_config(self):
        "Needed within TensorFlow to serialize this layer"
        config = {"dims_in": self.dims_in, "dims_c": self.dims_c}
        base_config = super().get_config()
        return dict(list(base_config.items()) + list(config.items()))


class DenseSubNet(tf.keras.layers.Layer):
    """
    Creates a dense subnetwork
    which can be used within the invertible modules.
    """

    def __init__(self, meta: Dict, channels_in: int, channels_out: int):
        """
        Args:
          meta:
            Dictionary with defining parameters
            to construct the network.
          channels_in_in:
            Number of input channels.
          channels_in_out:
            Number of output channels.
        """
        super().__init__()
        self.meta = meta
        self.channels_in = channels_in
        self.channels_out = channels_out

        # which activation
        if isinstance(meta["activation"], str):
            if meta["activation"] == "relu":
                activation = tf.keras.activations.relu
            elif meta["activation"] == "elu":
                activation = tf.keras.activations.elu
            elif meta["activation"] == "leakyrelu":
                activation = tf.keras.layers.LeakyReLU()
            elif meta["activation"] == "tanh":
                activation = tf.keras.activations.tanh
            else:
                raise ValueError(f'Unknown activation "{meta["activation"]}"')
        else:
            activation = meta["activation"]

        # Define the layers
        self.hidden_layers = [
            tf.keras.layers.Dense(
                self.meta["units"],
                activation=activation,
                kernel_initializer=self.meta["initializer"],
            )
            for i in range(self.meta["layers"])
        ]

        self.dense_out = tf.keras.layers.Dense(
            self.channels_out, kernel_initializer=self.meta["initializer"]
        )

    def _network(self, x):
        """The used layers in this Subnetwork.
        Returns:
          _layers (tf.keras.layers): Some stacked keras layers.
        """
        for layer in self.hidden_layers:
            x = layer(x)

        y = self.dense_out(x)
        return y

    def call(self, x):  # pylint: disable=W0221
        """
        Perform a forward pass through this layer.
        Args:
          x: input data (array-like of one or more tensors)
            of the form: ``x = input_tensor_1``.
        """
        out = self._network(x)
        return out
    

    def get_config(self):
        config = {
            "meta": self.meta,
            "channels_in": self.channels_in,
            "channels_out": self.channels_out,
        }
        base_config = super().get_config()
        return dict(list(base_config.items()) + list(config.items()))

    
    def build(self, input_shape):
        """
        Helps to prevent wrong usage of modules and helps for debugging.
        """
        assert (
            input_shape[-1] == self.channels_in
        ), f"Channel dimension of input ({input_shape[-1]}) and given input channels ({self.channels_in}) don't agree."

        super().build(input_shape)

    

class ConditionalFlow(tf.keras.Model):
    """Defines the conditional flow network"""

    def __init__(
        self,
        dims_in,
        dims_c,
        n_blocks,
        subnet_meta: Dict = None,
        subnet_constructor: callable =DenseSubNet,
        name="cflow",
        **kwargs,
    ):
        super().__init__(name=name, **kwargs)
        self.dims_in = dims_in
        self.dims_c = dims_c
        self.sum_dims = tuple(range(1, 1 + len(self.dims_in)))

        layer_list = []
        for i in range(n_blocks):
            layer_list.append(AllInOneBlock(dims_in, dims_c=dims_c, permute_soft=True, subnet_meta=subnet_meta, subnet_constructor=subnet_constructor))
        self.layer_list = layer_list

    def distribution_log_prob(self, inputs):
        log_z = tf.constant(0.5 * np.prod(self.dims_in) * np.log(2 * np.pi), dtype=tf.float32)
        neg_energy = -0.5 * tf.reduce_sum(inputs ** 2, axis =self.sum_dims)
        return neg_energy - log_z

    def call(self, x, c=None):
        y = x
        log_det = 0
        for layer in self.layer_list:
            y, ljd = layer(y, c=c)
            log_det += ljd
        return y, log_det

    def log_prob(self, x, context=None):
        noise, logabsdet = self.call(x, c=context)
        log_prob = self.distribution_log_prob(noise)
        return log_prob + logabsdet

    def sample(self, n_samples, c=None):
        shape = (n_samples,) + tuple(self.dims_c[0])
        z = tf.random.normal(shape)
        y = z
        for layer in self.layer_list[::-1]:
            y = layer(y, c=c, rev=True, jac=False)

        return y

    def prob_sample(self, n_samples, unfoldings, c=None):
        shape = (n_samples,) + tuple(self.dims_c[0])

        output = []
        for _ in range(unfoldings):
            z = tf.random.normal(shape)
            y = z
            for layer in self.layer_list[::-1]:
                y = layer(y, c=c, rev=True, jac=False)
            y = tf.expand_dims(y, axis=-1)
            output.append(y)
        
        sample = tf.concat(output, axis=-1)
        return sample
