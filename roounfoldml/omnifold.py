import sys
import numpy as np
import pandas as pd
import tensorflow as tf
import ROOT

from .dataloader import sanitise

def weighted_binary_crossentropy(y_true, y_pred):
    weights = tf.gather(y_true, [1], axis=1) # event weights
    y_true = tf.gather(y_true, [0], axis=1) # actual y_true for loss
    
    # Clip the prediction value to prevent NaN's and Inf's
    epsilon =  tf.keras.backend.epsilon()
    y_pred =  tf.keras.backend.clip(y_pred, epsilon, 1. - epsilon)

    t_loss = -weights * ((y_true) *  tf.keras.backend.log(y_pred) +
                         (1 - y_true) *  tf.keras.backend.log(1 - y_pred))
    
    return  tf.keras.backend.mean(t_loss)


class RooUnfoldReweight:
    def __init__(self, response, data, parameters=None):
        data = sanitise(data)
        assert data.shape[1] == response.n_input_features, "input features for data and response do not match"
        self.response = response
        self.data = data
        if parameters is None:
            self.model_one = self.build_step_one_model()
            self.model_two = self.build_step_two_model()
            self.iterations = 2
        elif isinstance(parameters, dict):
            self.iterations = parameters['iterations']
            self.model_one = parameters['model_one']
            self.model_two = parameters['model_two']
        self.compiled = False
        self.unfolded_data = None

    def build_step_one_model(self):
        # Define layer
        layer0 = tf.keras.layers.Dense(units=10, input_shape=[self.response.n_input_features], activation='relu')
        layer1 = tf.keras.layers.Dense(units=10,
                                       activity_regularizer=tf.keras.regularizers.l1_l2(l1=1e-5, l2=1e-3),
                                       kernel_regularizer=tf.keras.regularizers.l1_l2(l1=1e-5, l2=1e-3))
        layer2 = tf.keras.layers.Dense(units=10)
        layer3 = tf.keras.layers.Dense(units=1, activation='sigmoid')
        return tf.keras.Sequential([layer0, layer1, layer2, layer3])

    def build_step_two_model(self):
        # Define layer
        layer0 = tf.keras.layers.Dense(units=10, input_shape=[self.response.n_target_features], activation='relu')
        layer1 = tf.keras.layers.Dense(units=10,
                                       activity_regularizer=tf.keras.regularizers.l1_l2(l1=1e-5, l2=1e-3),
                                       kernel_regularizer=tf.keras.regularizers.l1_l2(l1=1e-5, l2=1e-3))
        layer2 = tf.keras.layers.Dense(units=10)
        layer3 = tf.keras.layers.Dense(units=1, activation='sigmoid')
        return tf.keras.Sequential([layer0, layer1, layer2, layer3])

    def fit_model(self, iterations):

        # set up models etc
        early = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=10)

        labels_mc = np.zeros(len(self.response.sim))  # number of mc events
        labels_data = np.ones(len(self.data))  # number of events in data

        xvals_1 = np.concatenate((self.response.sim, self.data))
        yvals_1 = np.concatenate((labels_mc, labels_data))

        xvals_2 = np.concatenate((self.response.gen, self.response.gen))
        yvals_2 = np.concatenate((labels_mc, np.ones(len(self.response.gen))))

        weights = np.empty(shape=(iterations, 2, len(self.response.sim)))

        # initial iterative weights are ones
        weights_pull = np.ones(len(self.response.sim))
        weights_push = np.ones(len(self.response.sim))

        self.model_one.compile(loss=weighted_binary_crossentropy, optimizer='Adam', metrics=['accuracy'])
        self.model_two.compile(loss=weighted_binary_crossentropy, optimizer='Adam', metrics=['accuracy'])
        for i in range(iterations):
            # first train the difference between data and reco
            weights_1 = np.concatenate((weights_push, np.ones(len(self.data))))
            self.model_one.fit(xvals_1, yvals_1, sample_weight=weights_1, batch_size=1000, epochs=1000, shuffle=False,
                               callbacks=[early], verbose=0)
            # extract the weights
            f = self.model_one.predict(self.response.sim, batch_size=10000)
            re_weights = f / (1. - f)
            re_weights = np.squeeze(np.nan_to_num(re_weights))
            weights_pull = weights_push * re_weights

            # then train the difference between truth and reweighted truth
            weights_2 = np.concatenate((np.ones(len(self.response.gen)), weights_pull))
            self.model_two.fit(xvals_2, yvals_2, batch_size=1000, epochs=1000, shuffle=False, callbacks=[early],
                               verbose=0)
            # and push the weights back
            f = self.model_two.predict(self.response.gen, batch_size=1000)
            re_weights = f / (1. - f)
            re_weights = np.squeeze(np.nan_to_num(re_weights))
            weights_push = re_weights
            self.weights = weights_push
        self.compiled = True

    def unfold(self):
        if not self.compiled:
            self.fit_model(self.iterations)
        #        self.unfolded_data = self.model.predict(self.data)
        self.unfolded_data = self.response.gen, self.weights*100. # not sure where this factor 100 is coming from..

    def Vunfold(self):
        if self.unfolded_data is None:
            self.unfold()
        return self.unfolded_data
    
    
    def Hunfold(self, feature, nbins=None, xmin=None, xmax=None):
        """returns histogram of unfolded feature"""
        if self.unfolded_data is None:
            self.unfold()
        if self.response.truth_labels:
            df = pd.DataFrame(data = self.unfolded_data[0], columns = self.response.truth_keys)
        else:
            print("I don't know which column that is, your response wasn't labeled.")       

        # if variable bins but a list
        if isinstance(nbins, list):
            nbins = np.asarray(nbins)
        
        # now if we have a variable width histogram.
        if isinstance(nbins,np.ndarray):
            bin_edges = nbins
            nbins = len(bin_edges)-1
            h = ROOT.TH1F("","",nbins,bin_edges)
    
        elif xmin or xmin==0:
            h = ROOT.TH1F("", "", nbins, xmin, xmax)
            bin_edges = np.linspace(xmin, xmax, nbins+1)
            
        elif nbins is None:
            _,bin_edges = np.histogram(df[feature]) #just get bin edges
            h = ROOT.TH1F("","",len(bin_edges)-1,bin_edges[0],bin_edges[-1])

        else:
            print("should be feature, nbins, xmin, xmax")
            print("or feature, bin_edges")
            print('or just feature')
            return
        unweighted,_ = np.histogram(df[feature].values, bin_edges)
        scale = unweighted.sum()
        bin_contents, _ = np.histogram(df[feature].values, bin_edges, weights = self.unfolded_data[1]*len(self.unfolded_data[1])/scale)
        
        [h.SetBinContent(i+1,b) for i,b in enumerate(bin_contents)]
        return h
