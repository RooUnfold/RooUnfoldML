=======
Credits
=======

Maintainer
----------

* vincent croft <vincent.croft@cern.ch>

Contributors
------------

None yet. Why not be the first? See: CONTRIBUTING.rst
