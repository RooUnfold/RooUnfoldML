===========
RooUnfoldML
===========

.. image:: https://img.shields.io/travis/vincecr0ft/roounfoldml.svg
        :target: https://travis-ci.org/vincecr0ft/roounfoldml

.. image:: https://img.shields.io/pypi/v/roounfoldml.svg
        :target: https://pypi.python.org/pypi/roounfoldml


A machine learning based extension to RooUnfold

* Free software: 3-clause BSD license
* Documentation: (COMING SOON!) https://vincecr0ft.github.io/roounfoldml.

Features
--------

* TODO
